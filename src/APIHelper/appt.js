import config from "./config.js";
import * as APIs from "../APIs";
export function appointmentAdd(parameter) {
  let { errorMessage } = config;
  return new Promise((resolve, reject) => {
    APIs.appointmentAdd(parameter)
      .then((res) => {
        resolve(res);
      })
      .catch((error) => {
        let { response } = error;
        if (response.data) {
          let { reason } = response.data.error;
          reject({ message: reason, error });
          return;
        }
        reject({ message: errorMessage, error });
      });
  });
}

export function appointmentGetDateTime(parameter) {
  let { errorMessage } = config;
  return new Promise((resolve, reject) => {
    APIs.appointmentGetDateTime(parameter)
      .then((res) => {
        let { code, datesBetween } = res;
        if (code === "0001") {
          resolve(datesBetween);
        } else {
          resolve([]);
        }
      })
      .catch((error) => {
        reject({ message: errorMessage, error });
      });
  });
}

export function appointmentGetList(parameter) {
  let { errorMessage } = config;
  return new Promise((resolve, reject) => {
    APIs.appointmentGetList(parameter)
      .then((res) => {
        resolve(res);
      })
      .catch((error) => {
        reject({ message: errorMessage, error });
      });
  });
}

export function appointmentEdit(parameter) {
  let { errorMessage } = config;
  return new Promise((resolve, reject) => {
    APIs.appointmentEdit(parameter)
      .then((res) => {
        resolve(res);
      })
      .catch((error) => {
        reject({ message: errorMessage, error });
      });
  });
}
