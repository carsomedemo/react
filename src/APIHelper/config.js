const CONFIG_SHARE = {
  errorMessage: "Opss, Something not right...",
};

const CONFIG_STAGE = {
  // Stage
  ...CONFIG_SHARE,
};

export default CONFIG_STAGE;
