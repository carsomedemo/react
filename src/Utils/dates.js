// import DeviceInfo from 'react-native-device-info';
import Constants from "../Constants";
import moment from "moment";

export function getDateformat() {
  return Constants.dateformat;
}

export function date(format = Constants.dateformat.normalDate) {
  return moment().format(format);
}

export function normalDate() {
  return moment().format(Constants.dateformat.normalDate);
}

export function longDate() {
  return moment().format(Constants.dateformat.longDate);
}

export function shortDate() {
  return moment().format(Constants.dateformat.shortDate);
}

export function dayDiff(startedAt, endedAt = "") {
  var dtEnd = new Date();
  var dtStart = moment(startedAt, "YYYY-MM-DD").toDate();
  dtEnd = endedAt.length > 0 ? moment(endedAt, "YYYY-MM-DD").toDate() : dtEnd;
  return moment(dtStart).diff(dtEnd, "days");
}

export function secondsToTime(ms) {
  let duration = moment.duration(ms, "milliseconds");
  let fromMinutes = Math.floor(duration.asMinutes());
  let fromSeconds = Math.floor(duration.asSeconds() - fromMinutes * 60);

  return Math.floor(duration.asSeconds()) >= 60
    ? (fromMinutes <= 9 ? "0" + fromMinutes : fromMinutes) +
        ":" +
        (fromSeconds <= 9 ? "0" + fromSeconds : fromSeconds)
    : "00:" + (fromSeconds <= 9 ? "0" + fromSeconds : fromSeconds);
}
