import { toast } from "react-toastify";
export function toastSuccess(msg, className = "success-toast btn--jump") {
  toast(msg, {
    type: "success",
    className,
    position: toast.POSITION.RIGHT_CENTER
  });
}

export function toastError(msg, className = "error-toast btn--jump") {
  toast(msg, {
    type: "warning",
    className,
    position: toast.POSITION.RIGHT_CENTER
  });
}
