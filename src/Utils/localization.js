import { renderToStaticMarkup } from "react-dom/server";

export function initialLocalize(props) {
  const languages = ["en", "cn"];
  const defaultLanguage = localStorage.getItem("languageCode") || languages[0];
  props.initialize({
    languages: [
      { name: "English", code: "en" },
      { name: "Bahasa", code: "bn" }
    ],
    options: {
      renderToStaticMarkup,
      defaultLanguage
    }
  });
}
