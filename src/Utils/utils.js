export function decodeHTMLEntity(html) {
  var cache = {};
  var character;
  var e = document.createElement("div");

  return html.replace(/([&][^&; ]+[;])/g, function(entity) {
    character = cache[entity];
    if (!character) {
      e.innerHTML = entity;
      if (e.childNodes[0])
        character = cache[entity] = e.childNodes[0].nodeValue;
      else character = "";
    }
    return character;
  });
}
