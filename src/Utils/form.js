import moment from "moment";

export function convertICGender(value) {
  var lastInt = parseInt(value.substr(value.length - 1), 10);
  var gender = lastInt % 2 === 0 ? "Female" : "Male";
  return gender;
}

export function convertICDOB(value, format) {
  var dob = moment(value.substr(0, 6), "YYMMDD").toDate();

  // Checking for >= 2000
  var year = moment().year() - 18;
  var yearDOB = moment(dob).year();
  var valid = yearDOB >= year;
  if (valid) {
    yearDOB -= 100;
    dob = moment(yearDOB + value.substr(2, 6), "YYYYMMDD").toDate();
  }
  // Checking for >= 2000

  dob = moment(dob).format(format);
  dob = dob === "Invalid date" ? "" : dob;
  return dob;
}

export function convertICAge(value) {
  var dob = moment(value, "YYYY-MM-DD").toDate();
  var age = moment().diff(dob, "years");
  return age;
}

export function converDOBAge(value) {
  var dob = moment(value, "YYYY-MM-DD").toDate();
  var age = moment().diff(dob, "years");
  return age;
}

export function isEmailValid(email) {
  let pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(String(email).toLowerCase());
}

export function isNRIC(value) {
  var patt = /^[0-9]{12}$/g;
  var pattDash = /^[0-9]{6} [0-9]{2} [0-9]{4}$/g;
  var res = patt.test(value) || pattDash.test(value);
  return res;
}
