/**
 * For error
 */
import * as APIs from "../../APIs";

function errorCatch(errorMsg) {
  return {
    type: "ERROR",
    errorMsg: errorMsg,
  };
}
// export function userLogin(parameter) {
//   return dispatch => {
//     return APIs.userLogin(parameter)
//       .then(res => {
//         dispatch(memberSet(res));
//       })
//       .catch(error => {
//         dispatch(errorCatch(error));
//       });
//   };
// }

export function memberSet(value) {
  return {
    type: "MEMBER",
    value: value,
  };
}
