let defaultState = {
  errorMsg: "",
  actions: "",
  member: ""
};

const mainReducer = (state = defaultState, action) => {
  // Targer empty reducer after clear cache
  var BreakException = {};
  var empty = true;
  try {
    Object.keys(state).forEach(key => {
      if (typeof state[key] === "boolean") {
        return;
      }
      var valid;
      valid = state[key] === "";
      valid = valid || state[key].length === 0;
      if (!valid) {
        empty = false;
        throw BreakException;
      }
    });
  } catch (e) {
    if (e !== BreakException) {
      throw e;
    }
  }

  if (empty) {
    return {
      ...state,
      actions: "EMPTY_REDUCER",
      errorMsg: "empty reducer."
    };
  }
  // Targer empty reducer after clear cache

  switch (action.type) {
    case "ERROR":
      return {
        ...state,
        actions: action.type,
        errorMsg: action.errorMsg
      };
    case "MEMBER":
      return {
        ...state,
        actions: action.type,
        member: action.value
      };
    default:
      return {
        ...state
      };
  }
};

export default mainReducer;
