const CONFIG_SHARE = {
  timeout: 60 * 1000, // 60 seconds waiting,
};

const CONFIG_STAGE = {
  // Stage
  apiUrl: "http://carsome-api.velotech.co",
  // apiUrl: "http://localhost:3000",
  mockUrl: "https://22d61136-828d-47fc-b168-a97128cc936c.mock.pstmn.io",
  mockApiKey:
    "PMAK-5eeecda78eec73004e53b465-a0045a3566f442240f4114aa6412661245",
  ...CONFIG_SHARE,
};

export default CONFIG_STAGE;
