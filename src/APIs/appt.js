import config from "./config.js";
import axios from "axios";
export function appointmentAdd(parameter) {
  // let url = Constants.API.URI
  let { apiUrl } = config;
  let url = apiUrl + "/v1/appointment/add";
  return new Promise((resolve, reject) => {
    axios({
      url: url,
      method: "post",
      data: parameter,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then(function (response) {
        let data = response.data;
        resolve(data);
      })
      .catch(function (error) {
        reject(error);
      });
  });
}

export function appointmentGetDateTime(parameter) {
  let { apiUrl } = config;
  let url = apiUrl + "/v1/appointment/datetime";
  return new Promise((resolve, reject) => {
    axios({
      url: url,
      method: "post",
      data: parameter,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then(function (response) {
        console.log(response);
        let data = response.data;
        resolve(data);
      })
      .catch(function (error) {
        reject(error);
      });
  });
}

export function appointmentGetList() {
  let { apiUrl, mockApiKey, mockUrl } = config;
  let url = apiUrl + "/v1/appointment";
  return new Promise((resolve, reject) => {
    axios({
      url: url,
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then(function (response) {
        let data = response.data;
        resolve(data);
      })
      .catch(function (error) {
        reject(error);
      });
  });
}

export function appointmentEdit(parameter) {
  let { apiUrl, mockApiKey, mockUrl } = config;
  let url = apiUrl + "/v1/appointment/edit";
  return new Promise((resolve, reject) => {
    axios({
      url: url,
      data: parameter,
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then(function (response) {
        let data = response.data;
        resolve(data);
      })
      .catch(function (error) {
        reject(error);
      });
  });
}
