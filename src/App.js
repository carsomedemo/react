import React, { Component } from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "bootstrap/dist/css/bootstrap.min.css";
// Custom CSS
import "./Styles/Main.scss";

import NavBar from "./Components/NavBar";

import { TransitionGroup, CSSTransition } from "react-transition-group";
import Home from "./Screens/Home";
import Account from "./Screens/Account";
import Form from "./Screens/Form";
import Table from "./Screens/Table";
import SignInUp from "./Screens/SignInUp";
import ErrorPage from "./Screens/ErrorPage";
import Loading from "./Screens/Loading";
import AppointmentList from "./Screens/AppoinmentList";

import { withLocalize } from "react-localize-redux";
import * as Utils from "./Utils";

import language from "./translation/app.json";

// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "./Redux/Actions/index.js";

import ReactGA from "react-ga";
import ClearCache from "react-clear-cache";
// import queryString from "query-string";
import Dialog from "./Modal/Dialog";

import Constants from "Constants";
// Toast
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "react-toastify/dist/ReactToastify.css";
import "./Scss/themes/light-theme/light.scss";

toast.configure();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chkbox: false,
      isLoading: false,
      switchon: false,
    };

    Utils.initialLocalize(props);
    this.props.addTranslation(language);

    const { REACT_APP_GA } = process.env;
    ReactGA.initialize(REACT_APP_GA);
    const { pathname, search } = window.location;
    ReactGA.pageview(pathname + search);
  }

  componentDidUpdate(prevProps) {
    var { activeLanguage } = prevProps;
    const prevLangCode = activeLanguage && activeLanguage.code;

    activeLanguage = this.props.activeLanguage;
    const valid = activeLanguage && activeLanguage.code;
    const curLangCode = valid ? activeLanguage.code : "en";

    const hasLanguageChanged = prevLangCode !== curLangCode;
    if (hasLanguageChanged) {
      localStorage.setItem("languageCode", curLangCode);
    }
  }

  render() {
    let navBar = <NavBar parentProps={this.props} />;
    return (
      <div className="App">
        {
          // <WebMaintanence switchon={this.state.switchon} />
        }
        <div className={this.state.switchon === true ? "routeoff" : "routeon"}>
          <ClearCache>
            {({ isLatestVersion, emptyCacheStorage }) => (
              <div>
                {!isLatestVersion && (
                  <Dialog
                    showModal={true}
                    onConfirm={() => {
                      emptyCacheStorage();
                    }}
                    maintitle="Newer version is available now!"
                    subtitle="Please update."
                    iconModal={null}
                  />
                )}
              </div>
            )}
          </ClearCache>
          <Router loader={"bar"} color={"#506CEA"} size={9}>
            <div className="main-wrap">
              {navBar}
              <Route
                render={({ location }) => (
                  <TransitionGroup className="transition-group">
                    <CSSTransition
                      key={location.key}
                      timeout={{ enter: 900, exit: 900 }}
                      classNames="fade"
                    >
                      <Switch>
                        <Route
                          exact
                          path={Constants.pathName.Home}
                          component={Home}
                        />
                        <Route
                          path={Constants.pathName.Account}
                          component={Account}
                        />
                        <Route
                          path={Constants.pathName.Form}
                          component={Form}
                        />
                        <Route
                          path={Constants.pathName.SignInUp}
                          component={SignInUp}
                        />
                        <Route
                          path={Constants.pathName.Table}
                          component={Table}
                        />
                        <Route
                          path={Constants.pathName.ErrorPage}
                          component={ErrorPage}
                        />
                        <Route
                          path={Constants.pathName.Loading}
                          component={Loading}
                        />
                        <Route
                          path={Constants.pathName.AppointmentList}
                          component={AppointmentList}
                        />
                      </Switch>
                    </CSSTransition>
                  </TransitionGroup>
                )}
              />
              {
                // <ScrollUpBtn />
              }
            </div>
          </Router>

          <div className="bg-img top">
            <div className="bg-float bg-img-1">
              <img src={require("./assets/images/Ellipse-1.png")} />
            </div>
          </div>

          <div className="bg-img bottom">
            <div className="bg-float bg-img-2">
              <img src={require("./assets/images/Ellipse-2.png")} />
            </div>
          </div>

          <div className="bottom-illustration">
            <img src={require("./assets/images/car.svg")} />
            <img src={require("./assets/images/key.svg")} />
          </div>

          <div className="footer">Copyright © All right reserved.</div>
        </div>
        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps, ActionCreators)(withLocalize(App));
