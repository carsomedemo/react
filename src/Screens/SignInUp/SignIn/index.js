import React, { Component } from "react";
// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import FormGroup from "react-bootstrap/FormGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";

import "./style.scss";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
  }

  componentDidMount() {}

  render() {
    return (
      <div className="wrapper">
        {/* <Translate id="signin" />; */}
        <Form horizontal>
          <FormGroup controlId="formHorizontalEmail">
            <Col className="label">Email</Col>
            <Col sm={12}>
              <FormControl
                type="email"
                placeholder="Email"
                className="forminput"
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="formHorizontalPassword">
            <Col className="label">Password</Col>
            <Col sm={12}>
              <FormControl
                type="password"
                placeholder="Password"
                className="forminput"
              />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col sm={12}>
              <Button className="buttonFormSubmit" type="submit">
                Sign in
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state;
};
export default connect(
  mapStateToProps,
  ActionCreators
)(withLocalize(SignIn));
