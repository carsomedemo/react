import React, { Component } from "react";
// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import FormGroup from "react-bootstrap/FormGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";

import "./style.scss";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
  }

  componentDidMount() {}

  render() {
    return (
      <div className="wrapper">
        {/* <Translate id="title" /> */}
        {/* <Translate id="signin" />; */}
        <Form horizontal>
          <FormGroup>
            <Col className="label">Name:</Col>
            <Col sm={12}>
              <FormControl type="name" className="forminput" />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col className="label">Date of birth:</Col>
            <Col sm={12}>
              <FormControl
                type="text"
                placeholder="(DD/MM/YYYY)"
                className="forminput"
              />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col className="label">Gender:</Col>
            <Col sm={12}>
              <FormControl type="text" className="forminput" />
            </Col>
          </FormGroup>

          <FormGroup controlId="formHorizontalEmail">
            <Col className="label">Email:</Col>
            <Col sm={12}>
              <FormControl
                type="email"
                placeholder="exmaple@abc.com"
                className="forminput"
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="formHorizontalPassword">
            <Col className="label">Password</Col>
            <Col sm={12}>
              <FormControl
                type="password"
                placeholder="Password"
                className="forminput"
              />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={12}>
              <Button className="buttonFormSubmit" type="submit">
                Sign in
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state;
};
export default connect(
  mapStateToProps,
  ActionCreators
)(withLocalize(SignUp));
