import React, { Component } from "react";
// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
// import Row from "react-bootstrap/Row";
// import Col from "react-bootstrap/Col";
// import Nav from "react-bootstrap/Nav";

import "../../Scss/themes/light-theme/light.scss";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";
import SignIn from "./SignIn";
import SignUp from "./SignUp";

class Home extends Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = {
      key: 1
    };
  }

  componentDidMount() {}

  handleSelect(key) {
    this.setState({ key });
  }

  render() {
    return (
      <div className="home-wrap">
        <div className="home-inner-wrap">
          {/* <Translate id="morning" /> */}
          <div className="sign-in-sign-up-wrap">
            <div className="tabgroup">
              <Tab.Container defaultActiveKey="first">
                <Tabs
                  activeKey={this.state.key}
                  onSelect={this.handleSelect}
                  id="controlled-tab-example"
                >
                  <Tab eventKey={1} title="Sign In">
                    <SignIn />
                  </Tab>
                  <Tab eventKey={2} title="Sign Up">
                    <SignUp />
                  </Tab>
                </Tabs>
              </Tab.Container>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state;
};
export default connect(
  mapStateToProps,
  ActionCreators
)(withLocalize(Home));
