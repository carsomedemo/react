import React, { Component } from "react";
// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import "./style.scss";

import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import FormGroup from "react-bootstrap/FormGroup";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";
import { Formik } from "formik";
import * as yup from "yup";
class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
    };
    this.props.addTranslation(language);
  }

  componentDidMount() {}

  render() {
    {
      /*
      <Translate id="title" />
      <a onClick={() => this.props.history.push(Constants.pathName.SignIn)}>
        <span>Goto Sign In</span>
      </a>
      */
    }

    return (
      <div>
        <div className="profile-wrapper">
          <div className="profile-inner">
            <div className="profile-inner-wrap">
              <div className="profile-pic-wrap">
                <div className="profile-pic">
                  <img src={require("../../assets/images/100.png")} alt="" />
                </div>
              </div>

              <div className="profile-name">
                <h6>Jane Doe</h6>
              </div>
            </div>

            <hr />

            <div className="detail-wrap">
              <Formik
                initialValues={{
                  email: "",
                  password: "",
                }}
                validationSchema={yup.object().shape({
                  name: yup.string().label("Name").required("Name required"),
                  email: yup
                    .string()
                    .label("Email")
                    .email("Email required")
                    .required(),
                  password: yup
                    .string()
                    .min(8, "Password must be at least 8 characters")
                    .required("Password required")
                    .matches(
                      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                      "Password at least contain 1 Alphabet, 1 Number and 1 Special Character"
                    ),
                })}
                onSubmit={(values) => {
                  this.userLogin(values);
                }}
              >
                {({
                  validateForm,
                  values,
                  handleChange,
                  handleBlur,
                  errors,
                  setFieldTouched,
                  touched,
                  isValid,
                  handleSubmit,
                }) => {
                  let { name, email, password } = values;
                  console.log(errors);
                  return (
                    <div className="personal-detail form-add">
                      <h5>Personal Detail</h5>
                      <Form horizontal>
                        <FormGroup className="form-group">
                          <Col className="label">Name</Col>
                          <Col sm={12} className="form-value">
                            <input
                              name={"name"}
                              value={name}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            ></input>
                          </Col>
                          {errors.name && touched.name ? (
                            <Col className="label">{errors.name}</Col>
                          ) : null}
                        </FormGroup>
                        <FormGroup className="form-group">
                          <Col className="label">Email</Col>
                          <Col sm={12} className="form-value">
                            <input
                              name={"email"}
                              value={email}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            ></input>
                          </Col>
                          {errors.email && touched.email ? (
                            <Col className="label">{errors.email}</Col>
                          ) : null}
                        </FormGroup>
                        <FormGroup className="form-group">
                          <Col className="label">Password</Col>
                          <Col sm={12} className="form-value">
                            <input
                              name={"password"}
                              value={password}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            ></input>
                          </Col>
                          {errors.password && touched.password ? (
                            <Col className="label">{errors.password}</Col>
                          ) : null}
                        </FormGroup>
                        <button onClick={() => handleSubmit()}>submit</button>
                      </Form>
                    </div>
                  );
                }}
              </Formik>
            </div>

            <hr />

            <div className="detail-wrap">Log out</div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state;
};
export default connect(mapStateToProps, ActionCreators)(withLocalize(Account));
