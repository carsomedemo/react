import React, { Component } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as Icons from "@fortawesome/free-solid-svg-icons";
import { withLocalize, Translate } from "react-localize-redux";
import language from "./language.json";
import "../../../Scss/themes/light-theme/light.scss";

class Rows extends Component {
  render() {
    let {
      order_id,
      order_no,
      status,
      agreement_status,
      upline_name,
      advance_payment,
      buy_for,
      profile,
      delivery_method,
      name,
      customer_name,
      phone,
      address,
      address_1,
      address_2,
      postcode,
      city,
      state,
      country,
      remark,
      approved_by,
      tracking_no,
      tracking_url,
      payment_status,
      product_amount,
      postage_fee,
      payment_amount,
      payment_receipt,
      created_at,
      products: [{ product_id, product_name, quantity, price }],
    } = this.props.item;
    return (
      <div className="table-row">
        <Row className="table-row-header">
          <Col lg={12} md={12} sm={12} xs={12} className="header">
            <div className="field">
              <label>Order No. : </label>
              <span>{order_no}</span>
            </div>
            <div className="field">
              <label>Payment Status: </label>
              <span>{payment_status}</span>
            </div>
            <div className="field">
              <label>Status: </label>
              <span>{status}</span>
            </div>

            <div className="field">
              <label>Created at: </label>
              <span>{created_at}</span>
            </div>
          </Col>
        </Row>

        <Row className="data no-gutters">
          <Col lg={10} md={12} sm={12} xs={12}>
            <div className="field-data">
              <div className="field">
                <label>Name: </label>
                <span>{name}</span>
              </div>

              <div className="field">
                <label>Customer Name: </label>
                <span>{customer_name}</span>
              </div>

              <div className="field">
                <label>Upline Name:</label>
                <span>{upline_name}</span>
              </div>
            </div>
          </Col>
          <Col lg={2} md={12} sm={12} xs={12} className="action buy">
            <Button>Action</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Rows;
