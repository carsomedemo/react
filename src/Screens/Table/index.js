import React, { Component } from "react";
// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import "./style.scss";
import "../../Scss/themes/light-theme/light.scss";

import moment from "moment";

import Rows from "./Rows/index.js";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";

import { DateRangePicker } from "react-dates";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
// import SignIn from "../SignIn";
// import SignUp from "../SignUp";
import * as Utils from "../../Utils";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";

import Moment from "react-moment";

class Table extends Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = {
      search: "",
      startDate: moment(),
      endDate: moment().add(1, "M"),
      focused: false,
      data: [
        {
          order_id: 391,
          order_no: "A000382",
          status: "approved",
          agreement_status: 0,
          upline_name: "PUA KAI XIAN (000055)",
          advance_payment: 0,
          buy_for: "customer",
          profile:
            "https://profilepic.oss-ap-southeast-3.aliyuncs.com/profile_picture_live/agentId-001180-1570802528-thumbnail.png",
          delivery_method: "Postage",
          name: "NG JING WEN (001180)",
          customer_name: "Ng Jing Wen",
          phone: "0174371683",
          address: "21, Jalan Enggang Indah 1,",
          address_1: "21, Jalan Enggang Indah 1,",
          address_2: "Taman Enggang Indah",
          postcode: "14120",
          city: "Simpang Ampat",
          state: "PULAU PINANG",
          country: "MALAYSIA",
          remark: "Stock on hand",
          approved_by: "000000",
          tracking_no: "5319053359738209\r\n",
          tracking_url:
            "https://www.tracking.my/dhl-ecommerce/5319053359738209\r\n",
          payment_status: "successful",
          product_amount: "940.00",
          postage_fee: "14.00",
          payment_amount: "954.00",
          payment_receipt:
            "https://paymentreceipt.oss-ap-southeast-3.aliyuncs.com/payment_receipt_live/orderNo-A000382-1559140509.png",
          created_at: "2019-05-29 22:35:09",
          products: [
            {
              product_id: 1,
              product_name: "package A",
              quantity: 5,
              price: 188,
            },
          ],
        },
        {
          order_id: 390,
          order_no: "A000381",
          status: "approved",
          agreement_status: 0,
          upline_name: "ANGELENE WONG (000012)",
          advance_payment: 0,
          buy_for: "customer",
          profile:
            "https://profilepic.oss-ap-southeast-3.aliyuncs.com/profile_picture_live/agentId-000140-1570761459-thumbnail.png",
          delivery_method: "Postage",
          name: "PANG SIAU YEN (000140)",
          customer_name: "Heng Alvie",
          phone: "01128202820",
          address: "no 6, jalan kenanga SD 9/7,",
          address_1: "no 6, jalan kenanga SD 9/7,",
          address_2: "",
          postcode: "52200",
          city: "Bandar Sri damansara",
          state: "WILAYAH PERSEKUTUAN KUALA LUMPUR",
          country: "MALAYSIA",
          remark: "",
          approved_by: "000000",
          tracking_no: "5319053359738109\r\n",
          tracking_url:
            "https://www.tracking.my/dhl-ecommerce/5319053359738109\r\n",
          payment_status: "successful",
          product_amount: "188.00",
          postage_fee: "8.00",
          payment_amount: "196.00",
          payment_receipt:
            "https://paymentreceipt.oss-ap-southeast-3.aliyuncs.com/payment_receipt_live/orderNo-A000381-1559140503.png",
          created_at: "2019-05-29 22:35:03",
          products: [
            {
              product_id: 1,
              product_name: "package A",
              quantity: 1,
              price: 188,
            },
          ],
        },
        {
          order_id: 389,
          order_no: "A000380",
          status: "approved",
          agreement_status: 0,
          upline_name: "Xue  Qi (000125)",
          advance_payment: 0,
          buy_for: "customer",
          profile: "",
          delivery_method: "Postage",
          name: "JOCELYN BEE YEOK YEAN (000287)",
          customer_name: "Sow Yi Shin",
          phone: "019-7560224",
          address: "01-01?Block B?Jalan Impian Indah 1",
          address_1: "01-01?Block B?Jalan Impian Indah 1",
          address_2: "Taman Impian Indah",
          postcode: "82000",
          city: "Pontian",
          state: "JOHOR",
          country: "MALAYSIA",
          remark: "",
          approved_by: "000000",
          tracking_no: "5319053359739709\r\n",
          tracking_url:
            "https://www.tracking.my/dhl-ecommerce/5319053359739709\r\n",
          payment_status: "successful",
          product_amount: "188.00",
          postage_fee: "8.00",
          payment_amount: "196.00",
          payment_receipt:
            "https://paymentreceipt.oss-ap-southeast-3.aliyuncs.com/payment_receipt_live/orderNo-A000380-1559140045.png",
          created_at: "2019-05-29 22:27:25",
          products: [
            {
              product_id: 1,
              product_name: "package A",
              quantity: 1,
              price: 188,
            },
          ],
        },
      ],
      pageSize: 20,
      manual: false,
      page: 0,
    };

    Utils.toastSuccess("Stay Home! Stay Safe!");
  }

  componentDidMount() {}

  handleSelect(key) {
    this.setState({ key });
  }

  handleShow = () => {
    this.setState({
      modalopen: !this.state.modalopen,
    });
  };

  render() {
    let {
      data,
      page,
      manual,
      total,
      pageSize,
      startDate,
      focused,
      endDate,
    } = this.state;
    const columns = [
      // {
      //   Header: "Action",
      //   width: 250,
      //   Cell: ({ row }) => {
      //     let { status } = row._original;
      //     return (
      //       <div>
      //         <Button
      //           onClick={e => {
      //             this.setState({
      //               showBlockAgent: true,
      //               currentRow: row._original
      //             });
      //           }}
      //         >
      //           Block
      //         </Button>
      //         <Button
      //           onClick={e => {
      //             this.setState({
      //               showUpdatePassword: true,
      //               currentRow: row._original
      //             });
      //           }}
      //         >
      //           Update Password
      //         </Button>
      //       </div>
      //     );
      //   }
      // },
      {
        Header: "Created at",
        accessor: "created_at",
        width: 150,
        Cell: (row) => (
          <div style={{ textAlign: "center" }}>
            <Moment format="D MMM YYYY hh:mm A" withTitle>
              {row.value}
            </Moment>
          </div>
        ),
      },
      {
        Header: "Status",
        accessor: "status",
        width: 150,
        Cell: (row) => <div style={{ textAlign: "center" }}>{row.value}</div>,
      },
      {
        Header: "Password",
        accessor: "password",
        width: 150,
        Cell: (row) => <div style={{ textAlign: "center" }}>{row.value}</div>,
      },
      {
        Header: "Login",
        accessor: "login",
        width: 150,
        Cell: (row) => <div style={{ textAlign: "center" }}>{row.value}</div>,
      },
      {
        Header: "Name",
        accessor: "name",
        width: 150,
        Cell: (row) => <div style={{ textAlign: "center" }}>{row.value}</div>,
      },
      {
        Header: "Level Display",
        accessor: "level_display",
        width: 150,
        Cell: (row) => <div style={{ textAlign: "center" }}>{row.value}</div>,
      },
      {
        Header: "Upline Login",
        accessor: "upline_login",
        width: 200,
        Cell: (row) => <div style={{ textAlign: "center" }}>{row.value}</div>,
      },
      {
        Header: "Upline Name",
        accessor: "upline_name",
        width: 200,
        Cell: (row) => <div style={{ textAlign: "center" }}>{row.value}</div>,
      },
    ];

    return (
      <div className="table-wrapper">
        <DateRangePicker
          startDate={startDate} // momentPropTypes.momentObj or null,
          startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
          endDate={endDate} // momentPropTypes.momentObj or null,
          endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
          isOutsideRange={() => false}
          onDatesChange={({ startDate, endDate }) =>
            this.setState({ startDate, endDate })
          } // PropTypes.func.isRequired,
          focusedInput={focused} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
          onFocusChange={(focusedInput) =>
            this.setState({ focused: focusedInput })
          } // PropTypes.func.isRequired,
          displayFormat={Utils.getDateformat().shortDate}
        />
        <div className="motor-section">
          {data && data.length > 0
            ? data.map((a) => {
                return <Rows item={a} />;
              })
            : null}
        </div>
        {/* <ReactTable
          data={data}
          page={page}
          manual={manual}
          pages={total}
          columns={columns}
          pageSize={pageSize}
          onFetchData={(state, instance) => {
            // this.setState(
            //   {
            //     page: pageSize !== state.pageSize ? 0 : state.page,
            //     pageSize: state.pageSize
            //   },
            //   () => {
            //     this.agentList(search);
            //   }
            // );
          }}
        /> */}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps, ActionCreators)(withLocalize(Table));
