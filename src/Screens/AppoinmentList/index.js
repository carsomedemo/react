import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as Icons from "@fortawesome/free-solid-svg-icons";
import ModalBooking from "../../Modal/ModalBooking";
import Dialog from "../../Modal/Dialog";
import * as APIHelper from "../../APIHelper";
import moment from "moment";
import Constants from "Constants";

class AppointmentList extends Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
    this.state = {
      modalopen: false,
      dialogopen: false,
      data: [],
    };
    this.appointmentGetList();
    this.onEdit = this.onEdit.bind(this);
    this.onTrash = this.onTrash.bind(this);
  }

  componentDidMount() {}

  async appointmentEdit(params) {
    try {
      let respApptEdit = await APIHelper.appointmentEdit(params);
      this.setState({ modalopen: false });
      this.appointmentGetList();
    } catch (e) {
      console.log(e);
    }
  }

  async appointmentGetList() {
    try {
      let respApptList = await APIHelper.appointmentGetList();
      this.setState({ data: respApptList });
    } catch (e) {
      console.log(e);
    }
  }

  onEdit(item) {
    this.setState({ currentItem: item, modalopen: true });
  }

  onTrash(item) {}

  render() {
    let { data, modalopen, currentItem } = this.state;
    return (
      <div className="container">
        <Row>
          <Col md={12}>
            <div className="wrap-title">
              <h4>Appointment List</h4>
            </div>
          </Col>

          <Col md={12}>
            <div className="appointment-wrap">
              {data.map((item) => {
                return (
                  <ListRow
                    item={item}
                    onEdit={this.onEdit}
                    onTrash={this.onTrash}
                  />
                );
              })}
            </div>
          </Col>

          <Col md={12}></Col>
        </Row>

        <ModalBooking
          item={currentItem}
          showModal={modalopen}
          onConfirm={(params) => this.appointmentEdit(params)}
          handleClose={() => this.setState({ modalopen: false })}
        />

        <Dialog
          showModal={this.state.dialogopen}
          onConfirm={this.handleConfirm}
          maintitle="Are you sure?"
          subtitle="Once you delete cannot retrieve back"
          iconModal={Icons.faExclamationCircle}
          onCancel={this.handleConfirm}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state;
};
export default connect(
  mapStateToProps,
  ActionCreators
)(withLocalize(AppointmentList));

const ListRow = ({ item, onEdit, onTrash }) => {
  let {
    owner = { phone: "", email: "", name: "" },
    slot = {
      location: "",
      area: "",
      center: "",
      date: "",
      time: "",
    },
    createdAt,
  } = item;
  let { phone = "", email = "", name = "" } = owner;
  let { location = "", area = "", center = "", date = "", time = "" } = slot;
  return (
    <Row className="appointment-row">
      <Col xs={12} md={2}>
        <ListItem subtitle="Name" value={name} />
      </Col>
      <Col xs={12} md={3}>
        <ListItem subtitle="Email" value={email} />
      </Col>
      <Col xs={12} md={3}>
        <ListItem
          subtitle="Scheduled Time & Date"
          value={
            moment(time, Constants.dateformat.time24).format(
              Constants.dateformat.time12
            ) +
            ", " +
            moment(date).format(Constants.dateformat.longDate)
          }
        />
      </Col>
      <Col xs={12} md={2}>
        <ListItem subtitle="Inspection centre" value={center} />
      </Col>
      <Col md={2}>
        <div className="action-btn">
          <a
            href=""
            onClick={(e) => {
              e.preventDefault();
              onEdit(item);
            }}
          >
            <FontAwesomeIcon icon={Icons.faEdit} />
          </a>
          <a
            href=""
            onClick={(e) => {
              e.preventDefault();
              onTrash(item);
            }}
          >
            <FontAwesomeIcon icon={Icons.faTrash} />
          </a>
        </div>
      </Col>
    </Row>
  );
};

const ListItem = ({ subtitle, value }) => (
  <div>
    <Row>
      <Col>
        <p className="list-title">{subtitle}</p>
        <p className="list-value">{value}</p>
      </Col>
    </Row>
  </div>
);
