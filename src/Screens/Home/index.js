import React, { Component } from "react";
// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import Slider from "react-slick";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import * as Icons from "@fortawesome/free-solid-svg-icons";

import "./style.scss";
import "../../Scss/themes/light-theme/light.scss";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";

// import SignIn from "../SignIn";
// import SignUp from "../SignUp";
import * as Utils from "../../Utils";
import Dialog from "../../Modal/Dialog";
import Loader from "../../Components/Loader";
import BookingForm from "../BookingForm";

class Home extends Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = {
      key: 1,
      modalopen: false,
    };

    // Utils.toastSuccess("Stay Home! Stay Safe!");
  }

  componentDidMount() {}

  handleSelect(key) {
    this.setState({ key });
  }

  handleShow = () => {
    this.setState({
      modalopen: !this.state.modalopen,
    });
  };

  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    {
      /*
        <div className="home-wrap">
          <div className="home-inner-wrap">
            <Translate id="morning" />
            <div className="sign-in-sign-up-wrap">
              <div className="tabgroup">
                <Tab.Container defaultActiveKey="first">
                  <Tabs
                    activeKey={this.state.key}
                    onSelect={this.handleSelect}
                    id="controlled-tab-example"
                  >
                    <Tab eventKey={1} title="Sign In">
                      <SignIn />
                    </Tab>
                    <Tab eventKey={2} title="Sign Up">
                      <SignUp />
                    </Tab>
                  </Tabs>
                </Tab.Container>
              </div>
            </div>
          </div>
        </div>
        <div className="home">Home</div>
        */
    }

    return (
      <div className="home-wrapper">
        {/* <Loader text="Please wait a moment" isLoading={true} /> */}
        <div className="container">
          <div className="page-content">
            <div className="page-intro">
              <div className="row">
                <Col md={12}>
                  <h3 className="title">SELL YOUR USED CAR TODAY!</h3>
                  <h4>Professional, Convenient and Fast</h4>
                </Col>
              </div>

              <div className="row">
                <Col md={12}>
                  <h6 className="subtitle">
                    Enter Your Details For A Free Valuation! Just 2 step!
                  </h6>
                </Col>
              </div>
            </div>

            <div>
              <Col md={12}>
                <BookingForm parentProps={this.props} />

                {/* <HomeColumn
                    title="Title"
                    desc="description"
                    handleModal={this.handleShow}
                  />
                  <HomeColumn title="Title" desc="description" />
                  <HomeColumn title="Title" desc="description" />
                  <HomeColumn title="Title" desc="description" />
                  <HomeColumn title="Title" desc="description" />
                  <HomeColumn title="Title" desc="description" /> */}
              </Col>
              <Dialog
                showModal={this.state.modalopen}
                onConfirm={this.handleShow}
                maintitle="Something went wrong.."
                subtitle="please kindly check again"
                iconModal={Icons.faTimesCircle}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps, ActionCreators)(withLocalize(Home));

const HomeColumn = ({ title, desc, handleModal }) => (
  <Col md={4} sm={12}>
    <div className="column-wrap">
      <div className="column-img">
        <img src={require("../../assets/images/500_400.png")} alt="" />
      </div>
      <div className="column-title">{title}</div>
      <div className="column-desc">{desc}</div>

      <div className="column-btn" onClick={handleModal}>
        More
      </div>
    </div>
  </Col>
);
