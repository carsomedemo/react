import React, { Component } from "react";
// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import "./style.scss";

import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import FormGroup from "react-bootstrap/FormGroup";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";

class Account extends Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
  }

  componentDidMount() {}

  render() {
    {
      /*
      <Translate id="title" />
      <a onClick={() => this.props.history.push(Constants.pathName.SignIn)}>
      <span>Goto Sign In</span>
    </a>
    */
    }

    return (
      <div>
        <div className="profile-wrapper">
          <div className="profile-inner">
            <div className="profile-inner-wrap">
              <div className="profile-pic-wrap">
                <div className="profile-pic">
                  <img src={require("../../assets/images/100.png")} alt="" />
                </div>
              </div>

              <div className="profile-name">
                <h6>Jane Doe</h6>
              </div>
            </div>

            <hr />

            <div className="detail-wrap">
              <div className="personal-detail">
                <h5>Personal Detail</h5>
                <Form horizontal>
                  <FormGroup className="form-group">
                    <Col className="label">Name</Col>
                    <Col sm={12} className="form-value">
                      Jane Doe
                    </Col>
                  </FormGroup>

                  <FormGroup className="form-group">
                    <Col className="label">Wechat ID</Col>
                    <Col sm={12} className="form-value">
                      iamjanedoe
                    </Col>
                  </FormGroup>

                  <FormGroup className="form-group">
                    <Col className="label">NRIC</Col>
                    <Col sm={12} className="form-value">
                      911111101111
                    </Col>
                  </FormGroup>

                  <FormGroup
                    className="form-group"
                    controlId="formHorizontalEmail"
                  >
                    <Col className="label">Phone Number</Col>
                    <Col sm={12} className="form-value">
                      0123456789
                    </Col>
                  </FormGroup>
                </Form>
              </div>
            </div>

            <hr />

            <div className="detail-wrap">Log out</div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state;
};
export default connect(mapStateToProps, ActionCreators)(withLocalize(Account));
