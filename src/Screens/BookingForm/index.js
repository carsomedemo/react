import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as Icons from "@fortawesome/free-solid-svg-icons";

import "../../Scss/themes/light-theme/light.scss";
import CarDetail from "../../Components/Booking/CarDetail";
import BookingInformation from "../../Components/Booking/BookingInformation";
import BookingSummary from "../../Components/Booking/BookingSummary";
import BookingThankYou from "../../Components/Booking/BookingThankYou";
import * as APIHelper from "../../APIHelper";
import * as Utils from "../../Utils";
import Constants from "Constants";
import OwnerDetails from "../../Components/Booking/OwnerDetails";

const CAR_DETAILS = 1;
const BOOKING_DETAILS = 2;
const OWNER_DETAILS = 3;
const BOOKING_CONFIRMATION = 4;
const THANK_YOU = 5;

class BookingForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      brand: "",
      model: "",
      mobile: "",
      year: "",
      area: "",
      center: "",
      currentStep: 1, // Default is Step 1
      location: "",
      time: "09:00",
      date: "12 June 2021",
      name: "",
      email: "",
      dates: [],
      date: "",
      times: [],
      time: "",
    };
    console.log(this.props.parentProps);

    this._next = this._next.bind(this);
    this._prev = this._prev.bind(this);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderDetails = this.renderDetails.bind(this);
    this.appointmentAdd = this.appointmentAdd.bind(this);
  }
  async appointmentAdd() {
    try {
      let {
        location,
        area,
        center,
        date,
        time,
        brand,
        model,
        year,
        mobile,
        email,
        name,
      } = this.state;
      let params = {
        slot: {
          location,
          area,
          center,
          date,
          time,
        },
        vehicle: {
          brand,
          model,
          year,
        },
        owner: {
          phone: mobile,
          email,
          name,
        },
      };
      console.log("params", params);
      let respAppt = await APIHelper.appointmentAdd(params);
      let { code, message } = respAppt;
      if (code === "0001") Utils.toastSuccess(message);
      else Utils.toastError(message);
      console.log("Constants.pathName.AppointmentList", Constants.pathName);
      this.props.parentProps.history.push(Constants.pathName.AppointmentList);
    } catch ({ message }) {
      Utils.toastError(message);
    }
  }
  _next() {
    let currentStep = this.state.currentStep;
    // If the current step is 1 or 2, then add one on "next" button click
    switch (currentStep) {
      case CAR_DETAILS:
        if (this.carDetailFormik) this.carDetailFormik.handleSubmit();
        break;
      case BOOKING_DETAILS:
        if (this.bookDetailsFormik) this.bookDetailsFormik.handleSubmit();
        break;
      case OWNER_DETAILS:
        if (this.ownerDetails) this.ownerDetails.handleSubmit();
        break;
      case BOOKING_CONFIRMATION:
        this.appointmentAdd();
        break;
      default:
        break;
    }
    //
    // currentStep = currentStep >= 3 ? 4 : currentStep + 1;
    // this.setState({
    //   currentStep: currentStep,
    // });
  }

  _prev() {
    let currentStep = this.state.currentStep;
    // If the current step is 2 or 3, then subtract one on "previous" button click
    currentStep = currentStep <= 1 ? 1 : currentStep - 1;
    this.setState({
      currentStep: currentStep,
    });
  }

  previousButton() {
    let currentStep = this.state.currentStep;
    if (currentStep !== 1) {
      return (
        <button
          className="btn btn-secondary prev-btn"
          type="button"
          onClick={this._prev}
        >
          Previous
        </button>
      );
    }
    return null;
  }

  renderButtonText() {
    let currentStep = this.state.currentStep;
    switch (currentStep) {
      case CAR_DETAILS:
        return "Next";
      case BOOKING_DETAILS:
        return "Confirm";
      case OWNER_DETAILS:
        return "Confirm";
      case BOOKING_CONFIRMATION:
        return "Make an appointment";
      default:
        return "";
    }
  }

  nextButton() {
    let currentStep = this.state.currentStep;
    if (currentStep < 5) {
      return (
        <div className="booking-btn-wrap">
          <a
            className="btn btn-primary booking-submit"
            type=""
            onClick={this._next}
          >
            {this.renderButtonText()}

            <span className="btnarrow">
              <FontAwesomeIcon icon={Icons.faArrowCircleRight} />
            </span>
          </a>
        </div>
      );
    }
    return null;
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    console.log(value);
    this.setState({
      [name]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    // const { email, username, password } = this.state;
    // alert(`Your registration detail: \n
    //        Email: ${email} \n
    //        Username: ${username} \n
    //        Password: ${password}`);
  };

  renderStepTitle() {
    let currentStep = this.state.currentStep;
    switch (currentStep) {
      case CAR_DETAILS:
        return "Car & Contact Details";
      case BOOKING_DETAILS:
        return "Booking Details";
      case OWNER_DETAILS:
        return "Owner Details";
      case BOOKING_CONFIRMATION:
        return "Booking Confirmation";
      default:
        return "";
    }
  }

  renderStepNumber() {
    let currentStep = this.state.currentStep;
    switch (currentStep) {
      case CAR_DETAILS:
        return "1";
      case BOOKING_DETAILS:
        return "2";
      case OWNER_DETAILS:
        return "3";
      case BOOKING_CONFIRMATION:
        return "4";
      default:
        return "";
    }
  }
  renderDetails() {
    let {
      currentStep,
      brand,
      model,
      mobile,
      year,
      location,
      area,
      center,
      name,
      email,
      times,
      time,
      dates,
      date,
    } = this.state;
    switch (currentStep) {
      case CAR_DETAILS:
        return (
          <CarDetail
            brand={brand}
            model={model}
            year={year}
            onFormik={(formik) => (this.carDetailFormik = formik)}
            onFormikSubmit={(values) => {
              this.setState({ ...values, currentStep: currentStep + 1 });
            }}
          />
        );
      case BOOKING_DETAILS:
        return (
          <BookingInformation
            onFormik={(formik) => (this.bookDetailsFormik = formik)}
            onFormikSubmit={(values) => {
              this.setState({ ...values, currentStep: currentStep + 1 });
            }}
            onAppointmentDatesTime={({ dates, times }) => {
              this.setState({ dates, times });
            }}
            date={date}
            time={time}
            dates={dates}
            times={times}
            location={location}
            area={area}
            center={center}
            time={time}
            date={date}
          />
        );
      case OWNER_DETAILS:
        return (
          <OwnerDetails
            onFormik={(formik) => (this.ownerDetails = formik)}
            onFormikSubmit={(values) => {
              this.setState({ ...values, currentStep: currentStep + 1 });
            }}
            mobile={mobile}
            name={name}
            email={email}
          />
        );
      case BOOKING_CONFIRMATION: {
        let {
          brand,
          model,
          mobile,
          location,
          time,
          date,
          name,
          email,
        } = this.state;
        return (
          <BookingSummary
            carbrand={brand}
            carmodel={model}
            mobileno={mobile}
            perferredlocation={location}
            time={time}
            date={date}
            name={name}
            email={email}
          />
        );
      }

      case THANK_YOU:
        return <BookingThankYou />;
      default:
        return null;
    }
  }
  render() {
    return (
      <div className="booking-wrap">
        <div className="booking-inner">
          <div className="booking-step-title">{this.renderStepTitle()}</div>
          <div className="step-num-bg">
            <p>{this.renderStepNumber()}</p>
          </div>
          <form onSubmit={this.handleSubmit}>{this.renderDetails()}</form>
          {this.previousButton()}
        </div>
        {this.nextButton()}
      </div>
    );
  }
}

export default BookingForm;

const FormInput = ({ text, value, handleChange }) => (
  <div>
    <Row>
      <Col md={12}>
        <label>{text}</label>
      </Col>
      <Col md={12}>
        <input value={value} onChange={handleChange} />
      </Col>
    </Row>
  </div>
);
