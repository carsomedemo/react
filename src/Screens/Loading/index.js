import React, { Component } from "react";
// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";

import { withLocalize } from "react-localize-redux";
import language from "./language.json";

import Button from "react-bootstrap/Button";

import Loader from "../../Components/Loader";

class ErrorPage extends Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
    this.state = {
      isLoad: true,
    };
  }

  componentDidMount() {}

  handleShow = () => {
    this.setState({
      isLoad: !this.state.isLoad,
    });
  };

  render() {
    return (
      <div className="container">
        <div className="error-main-wrapper">
          <div className="error-inner">
            <div className="img404">
              <img src={require("../../assets/images/404.png")} alt="" />
            </div>

            <div className="error-page-text">
              <h5>
                Back to somewhere safe <a href="/">here</a>
              </h5>
            </div>

            <Loader
              text="Please wait a moment"
              isLoading={this.state.isLoad}
              handleShow={this.handleShow}
            />
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state;
};
export default connect(
  mapStateToProps,
  ActionCreators
)(withLocalize(ErrorPage));
