import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";
import { withLocalize } from "react-localize-redux";

// import { logout } from "../Utils";

class ModalPop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalopen: true,
    };
  }

  handleClose = () => {
    this.setState({
      modalopen: !this.state.modalopen,
    });
  };

  render() {
    var {
      maintitle = "",
      subtitle = "",
      showModal,
      onConfirm,
      iconModal,
      onCancel,
    } = this.props;
    return (
      <div>
        <Modal show={showModal} onHide={this.handleClose} className="modalpop">
          <div className="modal-icon">
            <FontAwesomeIcon icon={iconModal} />
          </div>
          <h4>{maintitle}</h4>
          <p>{subtitle}</p>

          <Button variant="primary" onClick={onConfirm}>
            Confirm
          </Button>

          <a href="" onClick={onCancel}>
            Cancel
          </a>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps, ActionCreators)(withLocalize(ModalPop));
