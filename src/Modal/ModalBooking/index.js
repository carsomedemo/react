import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import * as APIHelper from "../../APIHelper";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import FormGroup from "react-bootstrap/FormGroup";

import { Formik } from "formik";
import * as yup from "yup";

// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";
import { withLocalize } from "react-localize-redux";

import FormInput from "../../Components/FormInput";
import Select from "../../Components/Select";

import moment from "moment";
import Constants from "Constants";
// import { logout } from "../Utils";

class ModalBooking extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      _id: "",
      brand: "",
      model: "",
      phone: "",
      location: "",
      time: "",
      date: "",
      name: "",
      email: "",
      dates: [],
      times: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.appointmentGetDateTime = this.appointmentGetDateTime.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    let { item } = nextProps;
    if (!item) {
      return;
    }
    let {
      _id,
      owner = { phone: "", email: "", name: "" },
      slot = { location: "", area: "", center: "", date: "", time: "" },
      vehicle = { brand: "", model: "", year: "" },
    } = item;
    let { location = "", area = "", center = "", date = "", time = "" } = slot;
    let { brand = "", model = "", year = "" } = vehicle;
    let { phone = "", email = "", name = "" } = owner;

    this.setState(
      {
        _id,
        location,
        area,
        center,
        date,
        time,
        brand,
        model,
        year,
        phone,
        email,
        name,
      },
      () => {
        this.appointmentGetDateTime({ location, area, center });
        if (this.formik) {
          this.formik.setFieldValue("location", location);
          this.formik.setFieldValue("area", area);
          this.formik.setFieldValue("center", center);
          this.formik.setFieldValue("date", date);
          this.formik.setFieldValue("time", time);
          this.formik.setFieldValue("brand", brand);
          this.formik.setFieldValue("model", model);
          this.formik.setFieldValue("year", year);
          this.formik.setFieldValue("phone", phone);
          this.formik.setFieldValue("email", email);
          this.formik.setFieldValue("name", name);
        }
      }
    );
  }

  filterTimes({ selectedDate, schedule }) {
    let times = schedule.filter((a) => {
      let { date, timeslot } = a;
      return (
        moment(selectedDate).format(Constants.dateformat.normalDate) ===
        moment(date).format(Constants.dateformat.normalDate)
      );
    });
    if (times && times.length > 0) {
      times = Object.values(times[0].timeslot).map((a) => {
        let { value } = a;
        return { value: value, label: value };
      });
    }
    return times;
  }

  async appointmentGetDateTime(params) {
    let { date, time } = this.state;
    try {
      let schedule = await APIHelper.appointmentGetDateTime(params);
      let dates = schedule.map((a) => {
        let { date } = a;
        return {
          ...a,
          value: moment(date).format(Constants.dateformat.normalDate),
          label: moment(date).format(Constants.dateformat.normalDate),
        };
      });
      let times = [];
      if (dates && dates.length > 0)
        times = this.filterTimes({ selectedDate: dates[0].value, schedule });

      if (this.formik) {
        this.formik.setFieldValue(
          "date",
          date ? date : dates.length > 0 ? dates[0].value : ""
        );
        this.formik.setFieldValue(
          "time",
          time ? time : times.length > 0 ? times[0].value : ""
        );
      }
      // onAppointmentDatesTime({ dates, times });
      console.log("appointmentGetDateTime dates", dates);
      this.setState({ dates, times, schedule });
    } catch (e) {
      console.log(e);
    }
  }

  handleChange(e) {
    const { name, value } = e.target;
    if (name === "date") {
      let { dates } = this.state;
      let times = this.filterTimes({ selectedDate: value, schedule: dates });
      this.setState({ times });
    }
    if (this.formik) {
      this.formik.handleChange(e);
    }
    this.setState({ [name]: value }, () => {
      if (this.formik) {
        let { values } = this.formik;
        let { location, area, center } = values;
        if (
          location &&
          area &&
          center &&
          (name === "location" || name === "center" || name === "area")
        ) {
          this.appointmentGetDateTime({ location, area, center });
        }
      }
    });
  }

  render() {
    var {
      maintitle = "",
      subtitle = "",
      showModal,
      iconModal,
      handleClose,
      onConfirm,
    } = this.props;
    let {
      _id,
      location,
      dates,
      times,
      area,
      center,
      date,
      time,
      brand,
      model,
      year,
      phone,
      email,
      name,
    } = this.state;
    return (
      <Formik
        initialValues={{
          location,
          area,
          center,
          date,
          time,
          brand,
          model,
          year,
          phone,
          email,
          name,
        }}
        validationSchema={yup.object().shape({
          name: yup.string().label("Name").required("Name required"),
          email: yup.string().label("Email").required("Email required").email(),
          phone: yup.string().label("Mobile").required("Mobile required"),
          location: yup
            .string()
            .label("Location")
            .required("Location required"),
          area: yup.string().label("Area").required("Area required"),
          center: yup.string().label("Center").required("Center required"),
          date: yup.string().label("Date").required("Date required"),
          time: yup.string().label("Time").required("Time required"),
          brand: yup.string().label("Car Brand").required("Car brand required"),
          model: yup.string().label("Car Model").required("Car model required"),
          year: yup.string().label("Year").required("Year required"),
        })}
        onSubmit={(values) => {
          let params = {
            id: _id,
            slot: {
              location,
              area,
              center,
              date,
              time,
            },
            vehicle: {
              brand,
              model,
              year,
            },
            owner: {
              phone,
              email,
              name,
            },
          };
          console.log("params", params);
          onConfirm(params);
        }}
      >
        {(formik) => {
          this.formik = formik;
          // onFormik(formik);
          let {
            validateForm,
            values,
            handleChange,
            handleBlur,
            errors,
            setFieldTouched,
            touched,
            isValid,
            handleSubmit,
          } = formik;
          let {
            location,
            area,
            center,
            date,
            time,
            brand,
            model,
            year,
            phone,
            email,
            name,
          } = values;
          return (
            <div>
              <Modal show={showModal} onHide={handleClose} className="">
                <div className="booking-modal-edit-form">
                  <form>
                    <Row>
                      <Col md={12}>
                        <div className="wrap-title">
                          <h4>Edit Appointment</h4>
                        </div>
                      </Col>
                      <Col md={12}>
                        <Row>
                          <Col md={4}>
                            <FormInput
                              name={"brand"}
                              text="Car Brand"
                              value={brand}
                              handleChange={this.handleChange}
                              errorText={
                                errors.brand && touched.brand
                                  ? errors.brand
                                  : ""
                              }
                            />
                          </Col>
                          <Col md={4}>
                            <FormInput
                              name={"model"}
                              text="Car Model"
                              value={model}
                              handleChange={this.handleChange}
                              errorText={
                                errors.model && touched.model
                                  ? errors.model
                                  : ""
                              }
                            />
                          </Col>
                          <Col md={4}>
                            <FormInput
                              name={"year"}
                              text="Year"
                              value={year}
                              handleChange={this.handleChange}
                              errorText={
                                errors.year && touched.year ? errors.year : ""
                              }
                            />{" "}
                          </Col>
                        </Row>
                        <Row>
                          <Col md={12}>
                            <FormInput
                              text="Select Preferred Location"
                              name={"location"}
                              value={location}
                              handleChange={this.handleChange}
                              errorText={
                                errors.location && touched.location
                                  ? errors.location
                                  : ""
                              }
                            />
                          </Col>
                          <Col md={6}>
                            <FormInput
                              text="Center"
                              name={"center"}
                              value={center}
                              handleChange={this.handleChange}
                              errorText={
                                errors.center && touched.center
                                  ? errors.center
                                  : ""
                              }
                            />
                          </Col>
                          <Col md={6}>
                            <FormInput
                              text="Area"
                              name={"area"}
                              value={area}
                              handleChange={this.handleChange}
                              errorText={
                                errors.area && touched.area ? errors.area : ""
                              }
                            />
                          </Col>
                          <Col md={6}>
                            <Select
                              text="Date"
                              name={"date"}
                              value={date}
                              options={dates}
                              handleChange={this.handleChange}
                              errorText={
                                errors.date && touched.date ? errors.date : ""
                              }
                            />
                          </Col>
                          <Col md={6}>
                            <Select
                              text="Time"
                              name={"time"}
                              value={time}
                              options={times}
                              handleChange={this.handleChange}
                              errorText={
                                errors.time && touched.time ? errors.time : ""
                              }
                            />
                          </Col>
                        </Row>
                      </Col>
                      <Col md={12}>
                        <Row>
                          <Col md={12}>
                            <FormInput
                              text="Name"
                              name={"name"}
                              value={name}
                              handleChange={this.handleChange}
                              errorText={
                                errors.name && touched.name ? errors.name : ""
                              }
                            />
                          </Col>
                          <Col md={12}>
                            <FormInput
                              text="Email"
                              name={"email"}
                              value={email}
                              handleChange={this.handleChange}
                              errorText={
                                errors.email && touched.email
                                  ? errors.email
                                  : ""
                              }
                            />
                          </Col>
                          <Col md={12}>
                            <FormInput
                              text="Mobile No."
                              name={"phone"}
                              value={phone}
                              handleChange={this.handleChange}
                              errorText={
                                errors.phone && touched.phone
                                  ? errors.phone
                                  : ""
                              }
                            />
                          </Col>
                        </Row>
                      </Col>
                    </Row>

                    <Col md={12}>
                      <div className="align-center">
                        <a
                          className="btn btn-primary cancel-btn"
                          onClick={handleClose}
                        >
                          Cancel
                        </a>

                        <a
                          className="btn btn-primary submit-btn"
                          onClick={handleSubmit}
                        >
                          Confirm
                        </a>
                      </div>
                    </Col>
                  </form>
                </div>
              </Modal>
            </div>
          );
        }}
      </Formik>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(
  mapStateToProps,
  ActionCreators
)(withLocalize(ModalBooking));
