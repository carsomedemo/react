const CONFIG_SHARE = {
  timeout: 60 * 1000 // 20 seconds waiting,
};

const CONFIG_LIVE = {
  // Live
  // urlAPI: "https://production.com/#/",
  ...CONFIG_SHARE
};

const CONFIG_STAGE = {
  // Stage
  // urlAPI: "http://localhost:3000/#/",
  ...CONFIG_SHARE
};

module.exports =
  process.env.NODE_ENV === "production" ? CONFIG_LIVE : CONFIG_STAGE;
