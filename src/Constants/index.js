module.exports = {
  dateformat: {
    normalDate: "YYYY-MM-DD",
    time24: "HH:mm:ss",
    time12: "hh:mm a",
    longDate: "Do MMMM, YYYY",
    shortDate: "Do MMM, YYYY",
  },
  states: [
    { value: "", label: "Select your state" },
    { value: "14", label: "WILAYAH PERSEKUTUAN" },
    { value: "12", label: "SELANGOR" },
    { value: "7", label: "PERAK" },
    { value: "9", label: "PULAU PINANG" },
    { value: "5", label: "MELAKA" },
    { value: "4", label: "NEGERI SEMBILAN" },
    { value: "1", label: "JOHOR" },
    { value: "2", label: "KEDAH" },
    { value: "3", label: "KELANTAN" },
    { value: "6", label: "PAHANG" },
    { value: "8", label: "PERLIS" },
    { value: "13", label: "TERENGGANU" },
    { value: "10", label: "SABAH" },
    { value: "11", label: "SARAWAK" },
    { value: "15", label: "W.P. LABUAN" },
    { value: "16", label: "W.P. PUTRA JAYA" },
  ],
  pathName: {
    Home: "/",
    SignInUp: "/SignInUp",
    Form: "/Form",
    Account: "/Account",
    Table: "/Table",
    ErrorPage: "/ErrorPage",
    Loading: "/Loading",
    AppointmentList: "/AppointmentList",
  },
};
