import React, { Component } from "react";
import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";

import "../../Scss/themes/light-theme/light.scss";

class Loader extends Component {
  render() {
    let visibility = this.props.isLoading ? "visible" : "hidden";
    var { text, handleShow } = this.props;
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: require("../../assets/lottie/full_load.json"),
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
      duration: 1000,
    };
    return (
      <div className={visibility + " loader"}>
        <div className="bg loader-bg">
          <Lottie options={defaultOptions} />
          <div className="loader-text">
            <span>{text}</span>
          </div>
          <div className="dismiss-btn">
            <Button onClick={handleShow}>Dismiss</Button>
          </div>
        </div>
      </div>
    );
  }
}

export default Loader;
