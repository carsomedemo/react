import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";

import "../../../Scss/themes/light-theme/light.scss";
import FormInput from "../../FormInput";

class BookingSummary extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Row>
        <Col md={6}>
          <Row>
            <Col md={6}>
              <FormInput
                text="Car Brand"
                value={this.props.carbrand}
                handleChange={this.props.handleChange}
              />
            </Col>
            <Col md={6}>
              <FormInput
                text="Car Model"
                value={this.props.carmodel}
                handleChange={this.props.handleChange}
              />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <FormInput
                text="Select Preferred Location"
                value={this.props.perferredlocation}
                handleChange={this.props.handleChange}
              />
            </Col>
            <Col md={6}>
              <FormInput
                text="Date"
                value={this.props.date}
                handleChange={this.props.handleChange}
              />
            </Col>
            <Col md={6}>
              <FormInput
                text="Time"
                value={this.props.time}
                handleChange={this.props.handleChange}
              />
            </Col>
          </Row>
        </Col>
        <Col md={6}>
          <Row>
            <Col md={12}>
              <FormInput
                text="Name"
                value={this.props.name}
                handleChange={this.props.handleChange}
              />
            </Col>
            <Col md={12}>
              <FormInput
                text="Email"
                value={this.props.email}
                handleChange={this.props.handleChange}
              />
            </Col>
            <Col md={12}>
              <FormInput
                text="Mobile No."
                value={this.props.mobileno}
                handleChange={this.props.handleChange}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default BookingSummary;
