import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";
import * as APIHelper from "../../../APIHelper";

import "../../../Scss/themes/light-theme/light.scss";
import FormInput from "../../FormInput";
import Select from "../../Select";
import { Formik } from "formik";
import * as yup from "yup";
import moment from "moment";
import Constants from "Constants";

class BookingInformation extends Component {
  constructor(props) {
    super(props);
    let { location, date, time, area, center, dates = [], times = [] } = props;
    this.state = {
      location,
      date,
      time,
      area,
      center,
      dates,
      times,
    };
    this.handleChange = this.handleChange.bind(this);
    this.appointmentGetDateTime = this.appointmentGetDateTime.bind(this);
  }

  filterTimes({ selectedDate, schedule }) {
    let times = schedule.filter((a) => {
      let { date, timeslot } = a;
      return (
        moment(selectedDate).format(Constants.dateformat.normalDate) ===
        moment(date).format(Constants.dateformat.normalDate)
      );
    });
    if (times && times.length > 0) {
      times = Object.values(times[0].timeslot).map((a) => {
        let { value } = a;
        return { value: value, label: value };
      });
    }
    return times;
  }

  async appointmentGetDateTime(params) {
    try {
      let { onAppointmentDatesTime } = this.props;
      let { date, time } = this.state;
      let schedule = await APIHelper.appointmentGetDateTime(params);
      let dates = schedule.map((a) => {
        let { date } = a;
        return {
          ...a,
          value: moment(date).format(Constants.dateformat.normalDate),
          label: moment(date).format(Constants.dateformat.normalDate),
        };
      });
      let times = [];
      if (dates && dates.length > 0)
        times = this.filterTimes({ selectedDate: dates[0].value, schedule });

      if (this.formik) {
        this.formik.setFieldValue(
          "date",
          date ? date : dates.length > 0 ? dates[0].value : ""
        );
        this.formik.setFieldValue(
          "time",
          time ? time : times.length > 0 ? times[0].value : ""
        );
      }
      onAppointmentDatesTime({ dates, times });
      this.setState({ dates, times, schedule });
    } catch (e) {}
  }

  handleChange(e) {
    const { name, value } = e.target;
    if (name === "date") {
      let { dates } = this.state;
      let times = this.filterTimes({ selectedDate: value, schedule: dates });
      console.log("times", times);
      this.setState({ times });
    }
    if (this.formik) {
      this.formik.handleChange(e);
    }
    this.setState({ [name]: value }, () => {
      if (this.formik) {
        let { values } = this.formik;
        let { location, area, center } = values;
        if (
          location &&
          area &&
          center &&
          (name === "location" || name === "center" || name === "area")
        ) {
          this.appointmentGetDateTime({ location, area, center });
        }
      }
    });
  }

  render() {
    let { location, date, time, dates, times, area, center } = this.state;
    let { onFormik, onFormikSubmit } = this.props;
    return (
      <Formik
        initialValues={{
          date,
          time,
          location,
          area,
          center,
        }}
        validationSchema={yup.object().shape({
          location: yup
            .string()
            .label("Location")
            .required("Location required"),
          area: yup.string().label("Area").required("Area required"),
          center: yup.string().label("Center").required("Center required"),
          date: yup.string().label("Date").required("Date required"),
          time: yup.string().label("Time").required("Time required"),
        })}
        onSubmit={(values) => onFormikSubmit(values)}
      >
        {(formik) => {
          this.formik = formik;
          onFormik(formik);
          let {
            validateForm,
            values,
            handleChange,
            handleBlur,
            errors,
            setFieldTouched,
            touched,
            isValid,
            handleSubmit,
          } = formik;
          let {
            location,

            area,
            center,
            time,
            date,
          } = values;

          // select options
          const options_location = [
            { value: "", label: "- Select -" },
            { value: "kuala lumpur", label: "Kuala Lumpur" },
            // { value: "selangor", label: "Selangor" },
          ];
          const scope_area = {
            "kuala lumpur": [
              { value: "", label: "- Select -" },
              { value: "kelana jaya", label: "Kelana Jaya" },
            ],
            selangor: [
              { value: "", label: "- Select -" },
              { value: "klang", label: "Klang" },
            ],
          };
          const options_area = scope_area[location];
          const scope_center = {
            "kelana jaya": [
              { value: "", label: "- Select -" },
              { value: "KSL", label: "KSL" },
            ],
            klang: [
              { value: "", label: "- Select -" },
              { value: "axora", label: "Axora" },
            ],
          };
          const options_center = scope_center[area];
          // select options

          return (
            <Row>
              <Col md={12}>
                <Row>
                  <Col md={4}>
                    <Select
                      name={"location"}
                      text="Location"
                      options={options_location}
                      value={location}
                      handleChange={this.handleChange}
                      errorText={
                        errors.location && touched.location
                          ? errors.location
                          : ""
                      }
                    />

                    {/*
                      <FormInput
                        name={"location"}
                        text="Select Preferred Location"
                        value={location}
                        handleChange={this.handleChange}
                        errorText={
                          errors.location && touched.location
                            ? errors.location
                            : ""
                        }
                      />
                      */}
                  </Col>
                  <Col md={4}>
                    <Select
                      name={"area"}
                      text="Area"
                      options={options_area}
                      value={area}
                      handleChange={this.handleChange}
                      errorText={errors.area && touched.area ? errors.area : ""}
                    />

                    {/*
                    <FormInput
                      name={"area"}
                      text="Area"
                      value={area}
                      handleChange={this.handleChange}
                      errorText={errors.area && touched.area ? errors.area : ""}
                    />
                    */}
                  </Col>
                  <Col md={4}>
                    <Select
                      name={"center"}
                      text="Center"
                      options={options_center}
                      value={center}
                      handleChange={this.handleChange}
                      errorText={
                        errors.center && touched.center ? errors.center : ""
                      }
                    />

                    {/*
                      <FormInput
                        name={"center"}
                        text="Center"
                        value={center}
                        handleChange={this.handleChange}
                        errorText={
                          errors.center && touched.center ? errors.center : ""
                        }
                      />
                      */}
                  </Col>
                </Row>
              </Col>
              <Col md={12}>
                <Row>
                  <Col md={6}>
                    <Select
                      name={"date"}
                      text="Date"
                      options={dates}
                      value={date}
                      handleChange={this.handleChange}
                      errorText={errors.date && touched.date ? errors.date : ""}
                    />
                  </Col>
                  <Col md={6}>
                    <Select
                      name={"time"}
                      text="Time"
                      options={times}
                      value={time}
                      handleChange={this.handleChange}
                      errorText={errors.time && touched.time ? errors.time : ""}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          );
        }}
      </Formik>
    );
  }
}

export default BookingInformation;
