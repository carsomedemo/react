import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";

import "../../../Scss/themes/light-theme/light.scss";
import FormInput from "../../FormInput";

class BookingThankYou extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "Please write an essay about your favorite DOM element.",
    };
  }

  render() {
    return (
      <Row className="booking-thankyou">
        <Col md={12}>
          <h5 class="thank-title">Thanks for choosing Carsome</h5>
          <p>We will contact you as soon as possible</p>
        </Col>
      </Row>
    );
  }
}

export default BookingThankYou;
