import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";

import "../../../Scss/themes/light-theme/light.scss";
import FormInput from "../../FormInput";
import { Formik } from "formik";
import * as yup from "yup";

class CarDetail extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {
      brand = "",
      model = "",
      year = "",
      onFormik,
      onFormikSubmit,
    } = this.props;
    return (
      <Formik
        initialValues={{
          brand,
          model,
          year,
        }}
        validationSchema={yup.object().shape({
          brand: yup.string().label("Car Brand").required("Car brand required"),
          model: yup.string().label("Car Model").required("Car model required"),
          year: yup.string().label("Year").required("Year required"),
        })}
        onSubmit={(values) => onFormikSubmit(values)}
      >
        {(formik) => {
          onFormik(formik);
          let {
            validateForm,
            values,
            handleChange,
            handleBlur,
            errors,
            setFieldTouched,
            touched,
            isValid,
            handleSubmit,
          } = formik;
          let { brand, model, mobile, year } = values;
          return (
            <Row>
              <Col md={4}>
                <FormInput
                  name={"brand"}
                  text="Car Brand"
                  value={brand}
                  handleChange={handleChange}
                  errorText={errors.brand && touched.brand ? errors.brand : ""}
                />
              </Col>
              <Col md={4}>
                <FormInput
                  name={"model"}
                  text="Car Model"
                  value={model}
                  handleChange={handleChange}
                  errorText={errors.model && touched.model ? errors.model : ""}
                />
              </Col>
              <Col md={4}>
                <FormInput
                  name={"year"}
                  text="Year"
                  value={year}
                  handleChange={handleChange}
                  errorText={errors.year && touched.year ? errors.year : ""}
                />
              </Col>
            </Row>
          );
        }}
      </Formik>
    );
  }
}

export default CarDetail;
