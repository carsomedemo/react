import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";
import * as APIHelper from "../../../APIHelper";

import "../../../Scss/themes/light-theme/light.scss";
import FormInput from "../../FormInput";
import Select from "../../Select";
import { Formik } from "formik";
import * as yup from "yup";
import moment from "moment";
import Constants from "Constants";

class OwnerDetails extends Component {
  constructor(props) {
    super(props);
    let { name, email, mobile } = props;
    this.state = {
      name,
      email,
      mobile,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    if (name === "date") {
      let { dates } = this.state;
      let times = this.filterTimes({ selectedDate: value, schedule: dates });
      console.log("times", times);
      this.setState({ times });
    }
    if (this.formik) {
      this.formik.handleChange(e);
    }
    this.setState({ [name]: value }, () => {
      if (this.formik) {
        let { values } = this.formik;
        let { location, area, center } = values;
        if (
          location &&
          area &&
          center &&
          (name === "location" || name === "center" || name === "area")
        ) {
          this.appointmentGetDateTime({ location, area, center });
        }
      }
    });
  }

  render() {
    let { name, email, mobile } = this.state;
    let { onFormik, onFormikSubmit } = this.props;
    return (
      <Formik
        initialValues={{
          name,
          email,
          mobile,
        }}
        validationSchema={yup.object().shape({
          name: yup.string().label("Name").required("Name required"),
          email: yup.string().label("Email").required("Email required").email(),
          mobile: yup.string().label("Mobile").required("Mobile required"),
        })}
        onSubmit={(values) => onFormikSubmit(values)}
      >
        {(formik) => {
          this.formik = formik;
          onFormik(formik);
          let {
            validateForm,
            values,
            handleChange,
            handleBlur,
            errors,
            setFieldTouched,
            touched,
            isValid,
            handleSubmit,
          } = formik;
          let { name, email, mobile, area } = values;

          return (
            <Row>
              <Col md={6}>
                <FormInput
                  name={"name"}
                  text="Name"
                  value={name}
                  handleChange={this.handleChange}
                  errorText={errors.name && touched.name ? errors.name : ""}
                />
              </Col>

              <Col md={6}>
                <FormInput
                  name={"email"}
                  text="Email"
                  value={email}
                  handleChange={this.handleChange}
                  errorText={errors.email && touched.email ? errors.email : ""}
                />
              </Col>
              <Col md={6}>
                <FormInput
                  name={"mobile"}
                  text="Mobile Phone"
                  value={mobile}
                  handleChange={this.handleChange}
                  errorText={
                    errors.mobile && touched.mobile ? errors.mobile : ""
                  }
                />
              </Col>
            </Row>
          );
        }}
      </Formik>
    );
  }
}

export default OwnerDetails;
