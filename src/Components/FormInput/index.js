import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";

import "../../Scss/themes/light-theme/light.scss";
import { text } from "@fortawesome/fontawesome-svg-core";

class FormInput extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var {
      text,
      value,
      type = "text",
      handleChange,
      name,
      errorText,
    } = this.props;
    return (
      <div className="booking-col">
        <Row>
          <Col md={12}>
            <div className="label">
              <label>{text}</label>
            </div>
          </Col>
          <Col md={12}>
            <div className="input">
              <input
                type={type}
                name={name}
                value={value}
                onChange={handleChange}
              />
            </div>
          </Col>
        </Row>
        <Col className="label error-text ">{errorText}</Col>
      </div>
    );
  }
}

export default FormInput;
