import React, { Component } from "react";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Lottie from "react-lottie";
import Button from "react-bootstrap/Button";

import "../../Scss/themes/light-theme/light.scss";
import { text } from "@fortawesome/fontawesome-svg-core";
class Select extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var {
      text,
      value,
      type = "text",
      handleChange,
      name,
      errorText,
      options = [],
    } = this.props;
    return (
      <div className="booking-col">
        <Row>
          <Col md={12}>
            <div className="label">
              <label>{text}</label>
            </div>
          </Col>
          <Col md={12}>
            <div className="input">
              <select name={name} value={value} onChange={handleChange}>
                {Object.keys(options).map((key, index) => (
                  <option
                    key={options[index].value}
                    value={options[index].value}
                  >
                    {options[index].label}
                  </option>
                ))}
              </select>
            </div>
          </Col>
        </Row>
        <Col className="label">{errorText}</Col>
      </div>
    );
  }
}

export default Select;
