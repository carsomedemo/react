import React from "react";
import { NavLink } from "react-router-dom";
import { Link } from "react-scroll";
import { LinkContainer } from "react-router-bootstrap";
import { Navbar, Container, NavDropdown, Nav, Dropdown } from "react-bootstrap";

// Redux, actions
import { connect } from "react-redux";
import * as ActionCreators from "Redux/Actions/index.js";
import { withLocalize, Translate } from "react-localize-redux";
import Constants from "Constants";

import language from "./language.json";

// import { logout } from "../Utils";

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.props.addTranslation(language);
  }

  componentDidMount() {
    let scrollWithOffset = (el, offset) => {
      const elementPosition = el.offsetTop - offset;
      window.scroll({
        top: elementPosition,
        left: 0,
        behavior: "smooth",
      });
    };
    this.setState({ scrollWithOffset });
  }

  closeNavbar() {
    if (window.matchMedia("screen and (max-width: 991px)").matches) {
      document.getElementById("collaspe-btn").click();
    }
  }

  pushTo(name) {
    this.props.parentProps.history.push("#/" + name);
  }

  render() {
    let { languages, setActiveLanguage, member } = this.props.parentProps;
    let name = "";
    if (member) {
      name = member.Name;
    }

    let title = this.props.parentProps.location.hash.replace("#/", "");
    title = title.length <= 0 ? "Home" : title;
    return (
      <React.Fragment>
        <Navbar
          sticky="top"
          id="navbar"
          expand="lg"
          className="navbar navbar-expand-lg main-navbar"
          collapseOnSelect={true}
        >
          <Container>
            {this.props.pageName === "home" ? (
              <Link
                activeClass="active"
                to="home"
                spy={true}
                smooth={true}
                offset={-200}
                duration={800}
                className="navbar-brand"
              >
                Axo<span>lot</span>
              </Link>
            ) : (
              <LinkContainer exact to="/">
                <Navbar.Brand className="navbar-brand main-nav">
                  <div className="logo">
                    <img
                      src={require("../../assets/images/logo-carsome.png")}
                      alt=""
                    />
                  </div>
                </Navbar.Brand>
              </LinkContainer>
            )}

            <Navbar.Toggle aria-controls="basic-navbar-nav" id="collaspe-btn" />

            <Navbar.Collapse id="basic-navbar-nav" className="nav-main">
              <Nav className="mr-auto">
                {this.props.pageName === "home" ? (
                  <Link
                    activeClass="active"
                    to="features"
                    className="nav-link"
                    onClick={this.closeNavbar}
                  >
                    Features
                  </Link>
                ) : (
                  <div className="navbar-nav topbar-nav">
                    {/*
                     <NavLink to="/" className="nav-link">
                       About Us
                     </NavLink>

                     <NavLink to="/" className="nav-link">
                       Insurance
                     </NavLink>

                     <NavLink to="/" className="nav-link">
                       Articles
                     </NavLink>

                     <NavLink to="/" className="nav-link">
                       Contact Us
                     </NavLink>
                     */}
                  </div>
                )}
              </Nav>

              <Nav className="highlight-btn">
                <NavLink
                  to={Constants.pathName.AppointmentList}
                  duration={800}
                  className="nav-link line-btn"
                  onClick={() => {
                    {
                    }
                  }}
                >
                  Appt List
                </NavLink>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return state;
};
export default connect(mapStateToProps, ActionCreators)(withLocalize(NavBar));
